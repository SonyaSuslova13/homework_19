﻿#include <iostream>

class Animal
{
public:
	virtual void Voice() const = 0;
};

class Dog : public Animal
{
public:
	void Voice() const override
	{
		std::cout << "Woof\n";
	}
};

class Cat : public Animal
{
public:
	void Voice() const override
	{
		std::cout << "Meow\n";
	}
};

class Cow : public Animal
{
public:
	void Voice() const override
	{
		std::cout << "Mu\n";
	}
};

int main()
{
	Dog a;
	a.Voice();

	Cat b;
	b.Voice();

	Cow c;
	c.Voice();

	Animal* array[3];
	array[0] = new Dog();
	array[1] = new Cat();
	array[2] = new Cow();

		return(0);
}